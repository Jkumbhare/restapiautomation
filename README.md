
Hi, 

Thanks for giving such a great opportunity to write a amazing code to show my potential.
Please refer below approach towards designing of framework and Instructions to run the program.

Approach ::
 
1. As given in Rules of Game, I choose the desired Automation tools (Java, TestNG ) and Build Tool(Maven) for Automation of these RestAPIs.
2. As we need to read data from 2 different files, I developed the functionaly in "com.gojek.utils" package to read data from 2 different files and make a dataprovider out of it.
3. Designed & developed the functionality to execute the RestAPI get request and return the JSON Response in "com.gojek.rest" package.
4. Also Designed & developed most complex functionality of comaparing JSON Response based on JSONElements, JSONObjects and JSONArray.
5. As mentioned, we need to compare the respnse for File1:Line1 & File2:Line1 & So on, Read the data from Dataprovider and passed to the RESTAPI functionality.
6. Test and Dataprovider is developed in TestRunner class inside "src/test/java" so that Test Execution will start from here.
7. After retrieving the response for both Request , I am passing these responses to JSONComparator function.
8.A fter comparison, it will return a boolean value as True/False, based on this I am printing the desired result as below in logs/app.log file and on console too.

	2019-10-03 09:13:04,546  INFO [TestNG-PoolService-3] rootCategory:42 - https://reqres.in/api/users/4 equals https://reqres.in/api/users/4
	2019-10-03 09:13:04,559  INFO [TestNG-PoolService-8] rootCategory:44 - https://reqres.in/api/users/9 not equals https://reqres.in/api/users/12

9. If user has entered an invalid URL in any of Files, it will log an error message in log file as well in console as below.

	2019-10-03 09:13:04,663 ERROR [TestNG-PoolService-7] rootCategory:44 - Error while executing the Service -> no protocol: /api/unknown/2
	2019-10-03 09:13:04,663  INFO [TestNG-PoolService-7] rootCategory:44 - https://reqres.in/api/users?page=5 not equals /api/unknown/2

10. This code is tested for more than 1000 entries, while it takes 150 seconds to execute it in sequentially and 25 seconds in parallel execution using  (parallel=true) attribute in dataprovider.


Instructions to run the project::

1. Through Eclipse :

	i. Unzip the RESTAPIAutomation.zip file. Keep the RESTAPIAutomation folder in your Eclipse workspace.
	ii. Right Click on project and click on Run As --> Maven Clean.
	iii. Right Click again on project and click on Run As --> Maven install.
	iv. Or you can run by doing Right Click on project and click on Run as -> Maven build. Enter "clean install" as Goal and Click on Run.
	v. Your project will start running and will give you desired output.
	
2. Through Command Line : 

	i. Unzip the RESTAPIAutomation.zip file. Keep the RESTAPIAutomation folder in your Eclipse workspace.
	ii. Go inside the RESTAPIAutomation folder and copy entire path of folder.
	iii. Open the command Prompt and go to the RESTAPIAutomation foler.
	iv. Type "mvn clean install" on command prompt and hit enter.
	v. Your project will start running and will give you desired output. 
