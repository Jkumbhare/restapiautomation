package com.gojek.runner;

import org.testng.annotations.Test;

import com.gojek.rest.JSONHandler;
import com.gojek.testbase.TestBase;
import com.gojek.utils.FileHandler;
import com.gojek.utils.Utility;

import org.json.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import java.io.IOException;
import java.util.List;

public class TestRunner extends TestBase{
	
	JSONHandler objJH;
	JSONObject respForFileOne;
	JSONObject respForFileTwo;
	
	String executinStartTime;
	String executinEndTime;
	
	
  public TestRunner() throws IOException {
		super();
		objJH = new JSONHandler();
	}


  @Test(dataProvider = "dp")
  public void ExecuteHTTPRequests(String urlFromFile1, String urlFromFile2) {
		  
	// *** Execute REST Service for File1 & File2 URL and compare the response***
	boolean compstatus = objJH.GenerateAndCompareResponses(urlFromFile1, urlFromFile2);
	  
	// *** Printing the desired output ****
	if (compstatus==true) {
		TestBase.log.info(urlFromFile1 + " equals " + urlFromFile2 );
	}else{
		TestBase.log.info(urlFromFile1 + " not equals " + urlFromFile2 );
	}
  }



  @DataProvider(name="dp",parallel=true)
  public String[][] GetRequestURLs() {

	  // *** Get content of file1 as List ****
	  String f1Name = getFileAbsolutePath(prop.getProperty("DataFile1"));
	  List<String> file1Data = FileHandler.getFileContentAsList(f1Name);
	  
	  // *** check that file1 contents are not empty ****
	  if(file1Data.isEmpty()) {
		  System.out.println("Can not continue the execution as there is no data in " + f1Name );
		  System.exit(0);
	  }
	  
	  // *** Get content of file2 as List ****
	  String f2Name = getFileAbsolutePath(prop.getProperty("DataFile2"));
	  List<String> file2Data = FileHandler.getFileContentAsList(f2Name);
	  
	  // *** check that file2 contents are not empty ****
	  if(file2Data.isEmpty()) {
		  System.out.println("Can not continue the execution as there is no data in " + f2Name );
		  System.exit(0);
	  }	  
	  
	  // ** Create an array of two list to pass as a dataprovider.
	  String[][] myArr = FileHandler.createArrayOfTwoList(file1Data,file2Data);
	  
	  return myArr;
	    
  }
  
  @BeforeClass
  public void StartTime() {
	  executinStartTime = Utility.GetCurrentTimeUsingCalender();
  }
  
  
  @AfterClass
  public void EndTime() {
	  
	  executinEndTime = Utility.GetCurrentTimeUsingCalender();
	  long  executionTime = Utility.GetTimeDifferenceinSeconds(executinStartTime,executinEndTime);
	  
	  System.out.println(" Total Time in Seconds -> " + executionTime );
	  
  }

}
