package com.gojek.testbase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import org.apache.log4j.Logger;

public class TestBase {
	
	public static Logger log;
	public static Properties prop;
	final String configPath = "configuration/config.properties";
		
	public TestBase() throws IOException {
		
		log = Logger.getLogger("rootCategory");
		
		// *** Get config files absolute path *** 
        String absoluteConfigPath = getFileAbsolutePath(configPath);
		
        // Load config file and retrieves the properties
		FileInputStream objFIS = new FileInputStream(new File(absoluteConfigPath));
		prop = new Properties();
		prop.load(objFIS);
		
	}
    	
	
	// *** Get file absolute path of a File in Maven ***
	public String getFileAbsolutePath(String relativePath) {
		ClassLoader classLoader;
		URL resource = null;
		
		try {
	        classLoader = getClass().getClassLoader();
	        resource = classLoader.getResource(relativePath);
		} catch (Exception e) {
			log.error("Error in retrieving the content of File");
		}
		
        return resource.getPath();
	}
	
}
