package com.gojek.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.gojek.testbase.TestBase;

public class FileHandler {

	
	
	// *** Function to return file data as List ***
	public static List<String> getFileContentAsList(String fileName){
		
		ArrayList<String> list = null;
		try {
			list = (ArrayList<String>) Files.readAllLines(new File(fileName).toPath(), Charset.defaultCharset() );
		} catch (IOException e) {
			TestBase.log.error("Error in retrieving the content of File --> " + fileName);
		}
		
		return list;
	}	
	
	
	public static String[][] createArrayOfTwoList(List<String> L1,List<String> L2){
		
		int arrSize;
		String fileNum ;
		
		if (L1.size() != L2.size()) {
			if (L1.size()>L2.size()) {
				arrSize = L2.size();
				fileNum = "File1";
			}else
			{
				arrSize = L1.size();
				fileNum = "File2";
			}
			System.out.println("No. of entries/Lines in both Files are not Same." + fileNum + "is having more requests.");
		}else {
				arrSize=L1.size();
			}
		
		String[][] returnArray = new String[arrSize][2];
		
		for (int i = 0; i < arrSize; i++) {
			
			returnArray[i][0] = L1.get(i) ;
			returnArray[i][1] = L2.get(i) ;
		}
		
		return returnArray;
		
	}
	
	
	
	
	
	
	
	
	
}
