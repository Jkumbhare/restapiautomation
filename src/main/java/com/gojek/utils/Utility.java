package com.gojek.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.gojek.testbase.TestBase;


public class Utility {
	
	// *** Get Current Time using Calendar ***
	public static String GetCurrentTimeUsingCalender() {
		
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		
		DateFormat dtf = new SimpleDateFormat("HH:mm:ss");
		String currentTime = dtf.format(date);
		
		return currentTime;
	}
	
	
	// *** Get Time difference in long datatype providing StartTime & EndTime ***
	public static long GetTimeDifferenceinSeconds(String StartTime, String EndTime) {
		
		Date sTime = null;
		Date eTime = null;
		SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss");
		
		try {
			sTime = SDF.parse(StartTime);
			eTime = SDF.parse(EndTime);
		}catch(ParseException pe) {
			TestBase.log.error("Error while calculating the Time difference");
		}
		
		long difference = eTime.getTime() - sTime.getTime();
		return (difference/1000);
	}
	
	
}
