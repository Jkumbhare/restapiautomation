package com.gojek.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import com.gojek.testbase.TestBase;

public class RESTClass {
		
	
	static JSONObject executeGETRequest(String requestURL) {
		
		final String USER_AGENT = "Mozilla/5.0";
		JSONObject objJSonResponse = null;
		URL objURL;
		HttpURLConnection con = null;
		
		try {

			// Create a URL of Request
			objURL = new URL(requestURL);
			
			//Open the URL to execute the Request
			con = (HttpURLConnection) objURL.openConnection();
	
			// optional default is GET
			con.setRequestMethod("GET");
	
			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setDoOutput(true);
	
			// Convert the response to BufferedReader
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		
			// Converting the JSON response to JSOnObject
			objJSonResponse = new JSONObject(in.readLine());
		
		}catch(Exception E) {
			TestBase.log.error("Error while executing the Service -> " + E.getMessage());
		}
		finally
		{
			if (con != null)
				con.disconnect();
		}  
		return objJSonResponse;
	}
	
	
}
