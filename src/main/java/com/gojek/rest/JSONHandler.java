package com.gojek.rest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONHandler {
		

	// *** Function to compare two JSON objects  ****
	
	private boolean  compareJSONObjects (JSONObject js1, JSONObject js2) throws JSONException {
		
	    if (js1 == null || js2 == null) {
	        return (js1 == js2);
	    }

	    List<String> l1 =  Arrays.asList(JSONObject.getNames(js1));
	    Collections.sort(l1);
	    List<String> l2 =  Arrays.asList(JSONObject.getNames(js2));
	    Collections.sort(l2);
	    if (!l1.equals(l2)) {
	        return false;
	    }
	    
	    for (String key : l1) {
	        Object val1 = js1.get(key);
	        Object val2 = js2.get(key);
	        
	        //****** Code is to handle comparison of JSONObjects ******	
	        
	        if (val1 instanceof JSONObject) {
	            if (!(val2 instanceof JSONObject)) {
	                return false;
	            }
	            if (!compareJSONObjects((JSONObject)val1, (JSONObject)val2)) {
	                return false;
	            }
	        }
	        
	        //****** Code is to handle comparison of JSONArray ******
	        
	        if (val1 instanceof JSONArray) {
	            if (!(val2 instanceof JSONArray)) {
	                return false;
	            }
	            
	            int arrLen1 = ((JSONArray) val1).length();
	            int arrLen2 = ((JSONArray) val2).length();
	            
	            if (arrLen1 != arrLen2) {
	            	return false;
	            }
	            
	             for(int jsonArrLoop=0;jsonArrLoop<arrLen1;jsonArrLoop++) {
	            	 
	            	 String valueOfArrVal1 = ((JSONArray) val1).get(jsonArrLoop).toString();
	            	 JSONObject objValueOfArrVal1 = new JSONObject(valueOfArrVal1);
	            	 
	            	 String valueOfArrVal2 = ((JSONArray) val2).get(jsonArrLoop).toString();
	            	 JSONObject objValueOfArrVal2 = new JSONObject(valueOfArrVal2);
	            	 
	 	            if (!compareJSONObjects(objValueOfArrVal1, objValueOfArrVal2)) {
		                return false;
		            }
	             }
	        }

	        if (val1 == null) {
	            if (val2 != null) {
	                return false;
	            }
	        }  else if (!(val1 instanceof JSONObject) && !(val1 instanceof JSONArray) && !val1.equals(val2)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	// *** Function to compare two JSON objects  ****
	public boolean GenerateAndCompareResponses(String urlFromFile1, String urlFromFile2) {
		
		// *** To handle empty request url  ***
		if (urlFromFile1.isEmpty() || urlFromFile2.isEmpty()) {
			return false;
		}
		
		JSONObject objJSonResponse1 = RESTClass.executeGETRequest(urlFromFile1);
		JSONObject objJSonResponse2 = RESTClass.executeGETRequest(urlFromFile2);
		
		return compareJSONObjects(objJSonResponse1, objJSonResponse2);
	}	
	

}
